#include "Poynomial.h"

Polynomial::Polynomial() {
	pHead = NULL;
}

void Polynomial::insert(Node* m) {
	if (pHead == NULL) pHead = m;
	else {
		Node* cur = pHead;
		while (cur->pNext != NULL) cur = cur->pNext;
		cur->pNext = m;
	}
}

void Polynomial::input(ifstream &file) {
	while (!file.eof()) {
		Node* m = new Node;
		m->parameter = 1;
		m->variable = NULL;
		m->index_number = NULL;
		m->n = 0;
		m->pNext = NULL;
		char ch;
		ch = preview(file);
		bool flag = 0;
		if (ch == '-') {
			flag = 1;
			file.get();
			ch = preview(file);
		}
		if (ch >= 48 && ch <= 57) {
			float p;
			file >> p;
			m->parameter = p;
			if (file.eof()) {
				insert(m);
				break;
			}
		}
		if (flag) m->parameter = -m->parameter;
		if (preview(file) == '*') file.get();
		ch = file.get();
		int n = 0;
		while (ch != '+' && ch != '-' && !file.eof()) {
			add<char>(m->variable, m->n, ch);
			ch = file.get();
			if (ch == '*') ch = file.get();;
			if (ch == '^') {
				int i;
				file >> i;
				int tmp = m->n - 1;
				add<int>(m->index_number, tmp, i);
				ch = file.get();
				if (ch == '*') ch = file.get();;
			}
			else {
				int tmp = m->n - 1;
				add<int>(m->index_number, tmp, 1);
			}
			++n;
		}
		m->n = n;
		if (ch == '-') file.seekg(-1, file.cur);
		insert(m);
	}
}

void Polynomial::ouput(ofstream &file) {
	bool flag = 0;
	Node* cur = pHead;
	while (cur != NULL) {
		if (cur->parameter >= 0 && flag) file << '+';
		file << cur->parameter;
		for (int i = 0; i < cur->n; ++i) {
			file << '*';
			file << cur->variable[i];
			if (cur->index_number[i] != 1) {
				file << '^';
				file << cur->index_number[i];
			}
		}
		flag = 1;
		cur = cur->pNext;
	}
}

char preview(ifstream &file) {
	char ch = file.get();
	file.seekg(-1, file.cur);
	return ch;
}

template<class T> void add(T* &arr, int &n, T e) {
	T* tmp = new T[n];
	for (int i = 0; i < n; ++i) tmp[i] = arr[i];
	delete[] arr;
	++n;
	arr = new T[n];
	for (int i = 0; i < n - 1; ++i) arr[i] = tmp[i];
	arr[n - 1] = e;
	delete[] tmp;
}

// Thinh

void Polynomial::swap(Node *&p1, Node *&p2)
{
	Node *tmp = new Node;

	int lengthP1 = p1->n;
	tmp->variable = new char[lengthP1];
	tmp->index_number = new int[lengthP1];
	tmp->parameter = p1->parameter;
	tmp->n = p1->n;
	for (int i = 0; i<lengthP1; i++)
		tmp->variable[i] = p1->variable[i];
	for (int i = 0; i<lengthP1; i++)
		tmp->index_number[i] = p1->index_number[i];

	int lengthP2 = p2->n;
	delete[]p1->variable;
	delete[]p1->index_number;
	p1->variable = new char[lengthP2];
	p1->index_number = new int[lengthP2];
	p1->parameter = p2->parameter;
	p1->n = p2->n;
	for (int i = 0; i<lengthP2; i++)
		p1->variable[i] = p2->variable[i];
	for (int i = 0; i<lengthP2; i++)
		p1->index_number[i] = p2->index_number[i];


	int lengthTMP = tmp->n;
	delete[]p2->variable;
	delete[]p2->index_number;
	p2->variable = new char[lengthTMP];
	p2->index_number = new int[lengthTMP];
	p2->n = tmp->n;
	p2->parameter = tmp->parameter;
	for (int i = 0; i<lengthP1; i++)
		p2->variable[i] = tmp->variable[i];
	for (int i = 0; i<lengthP1; i++)
		p2->index_number[i] = tmp->index_number[i];
}


Polynomial& Polynomial::StandardizePoly()
{
	Node *tmp1;
	Node *tmp2;

	if (pHead == NULL || pHead->pNext == NULL) return *this;
	//return *this;


	for (tmp1 = this->pHead; tmp1->pNext != NULL; tmp1 = tmp1->pNext)
	{
		for (tmp2 = tmp1->pNext; tmp2 != NULL; tmp2 = tmp2->pNext)
		{

			int lengthTmp1 = tmp1->n;
			int SumTmp1 = 0;
			for (int i = 0; i<lengthTmp1; i++)
				SumTmp1 = SumTmp1 + tmp1->index_number[i];

			int lengthTmp2 = tmp2->n;
			int SumTmp2 = 0;
			for (int i = 0; i<lengthTmp2; i++)
				SumTmp2 = SumTmp2 + tmp2->index_number[i];

			if (SumTmp1>SumTmp2)
				swap(tmp1, tmp2);
		}
	}


	// SAP XEP VI TRI CAC KI TU CUNG SO MU
	for (tmp1 = this->pHead; tmp1->pNext != NULL; tmp1 = tmp1->pNext)
	{
		for (tmp2 = tmp1->pNext; tmp2 != NULL; tmp2 = tmp2->pNext)
		{
			bool check = true;

			int length1 = tmp1->n;
			int length2 = tmp2->n;
			int TongIndex1 = 0; // TONG GIA SO MU TRONG DON THUC 1
			int TongIndex2 = 0; // TONG GIA SO MU TRONG DOC THUC 2

			for (int i = 0; i<length1; i++)   // DEM TONG CUA DON THUC 1
				TongIndex1 = TongIndex1 + tmp1->index_number[i];

			for (int i = 0; i<length2; i++)   // DEM TONG CUA DON THUC 2
				TongIndex2 = TongIndex2 + tmp2->index_number[i];

			if (length1 != length2 || TongIndex1 != TongIndex2) // Chi so sanh neu cung do dai bien va co cung Tong so mu
				check = false;

			//this->print();

			//cout<<endl<<"------FINAL-----"<<endl;
			if (check == true) // NEU THOA THI TA SO SANH
			{
				int TongIntOfVariable1 = 0;
				int TongIntOfVariable2 = 0;
				for (int i = 0; i<length1; i++) // DEM TONG INT CUA DON THUC 1
					TongIntOfVariable1 = TongIntOfVariable1 + int(tmp1->variable[i]);
				for (int i = 0; i<length2; i++) // DEM TONG INT CUA DON THUC 2
					TongIntOfVariable2 = TongIntOfVariable2 + int(tmp2->variable[i]);
				if (TongIntOfVariable1>TongIntOfVariable2)
					swap(tmp1, tmp2);
			}
		}
	}
	return *this;
}

// Tri

Polynomial& Polynomial::operator=(Polynomial a) {
	if (a.pHead != NULL) {
		this->pHead = new Node;
		this->pHead->pNext = NULL;
		int x = a.pHead->n;
		this->pHead->n = x;
		this->pHead->variable = new char[x];
		this->pHead->index_number = new int[x];
		this->pHead->parameter = a.pHead->parameter;
		for (int i = 0; i < x; i++) {
			this->pHead->variable[i] = a.pHead->variable[i];
			this->pHead->index_number[i] = a.pHead->index_number[i];
		}
	}
	Node* cur = a.pHead->pNext;
	Node* cur2 = this->pHead;
	while (cur != NULL)
	{
		cur2->pNext = new Node;
		int y = cur->n;
		cur2->pNext->n = y;
		cur2->pNext->parameter = cur->parameter;
		cur2->pNext->variable = new char[y];
		cur2->pNext->index_number = new int[y];
		for (int i = 0; i < y; i++) {
			cur2->pNext->variable[i] = cur->variable[i];
			cur2->pNext->index_number[i] = cur->index_number[i];
		}
		cur2 = cur2->pNext;
		cur2->pNext = NULL;
		cur = cur->pNext;
	}
	return *this;
}

Polynomial Polynomial::add2Polynomial(Polynomial a) {
	Polynomial cur1, cur2;
	cur1 = *this;
	cur2 = a;
	Node *cur = cur1.pHead;
	while (cur->pNext != NULL) cur = cur->pNext;
	cur->pNext = cur2.pHead;
	cur1.compactPolynomial();
	cur1.StandardizePoly();	
	return cur1;
}
Polynomial Polynomial::subtract2Polynomial(Polynomial a) {
	Polynomial cur1, cur2;
	cur1 = *this;
	cur2 = a;
	Node *curr = cur2.pHead;
	while (curr != NULL) {
		curr->parameter *= (-1);
		curr = curr->pNext;
	}
	Node *cur = cur1.pHead;
	while (cur->pNext != NULL) cur = cur->pNext;
	cur->pNext = cur2.pHead; 
	cur1.compactPolynomial();	
	cur1.StandardizePoly();		
	return cur1;
}

// Tin

void Polynomial::deleteKey(int key)
{
	if (pHead == NULL) {
		return;
	}
	if (pHead->parameter == key) {
		Node* temp = pHead;
		pHead = pHead->pNext;
		delete temp;
	}
	Node* temp = pHead;
	Node* prev = NULL;
	// If head node itself holds the key or multiple occurrences of key
	while (temp != NULL && temp->parameter == key) {
		pHead = temp->pNext;
		temp = pHead;
	}

	while (temp != NULL) {
		if (temp->parameter == key) {
			prev->pNext = temp->pNext;
		}
		else {
			prev = temp;
		}
		temp = temp->pNext;
	}
	if (pHead == NULL) {
		pHead = new Node;
		pHead->pNext = NULL;
		pHead->parameter = 0;
		pHead->n = 0;
	}
}

bool Polynomial::isThatType(Node *&a, Node *&b)
{
	if (a->n != b->n)
		return false;
	for (int i = 0; i < a->n; i++) {
		if (a->variable[i] != b->variable[i])
			return false;
		if (a->index_number[i] != b->index_number[i])
			return false;
	}
	return true;
}

Polynomial& Polynomial::compactPolynomial()
{
	Polynomial l;
	float tmp;
	if (pHead == NULL)
		return *this;
	deleteKey(0);
	Node *cur1 = pHead;
	Node *cur2;
	deleteKey(0);
	while (cur1->pNext != NULL) {
		if (cur1->flag == true) cur1 = cur1->pNext;
		else {
			tmp = cur1->parameter;
			for (cur2 = cur1->pNext; cur2 != NULL; cur2 = cur2->pNext)
			{
				if (isThatType(cur1, cur2) == true) {
					tmp = tmp + cur2->parameter;
					cur2->flag = true; //�? r�t g?n r?i. 
				}
			}
			if (l.pHead == NULL)
			{
				l.pHead = new Node;
				l.pHead->parameter = tmp;
				l.pHead->n = cur1->n;
				l.pHead->variable = new char[cur1->n];
				l.pHead->index_number = new int[cur1->n];
				for (int i = 0; i < cur1->n; i++)
				{
					l.pHead->variable[i] = cur1->variable[i];
					l.pHead->index_number[i] = cur1->index_number[i];
				}
				l.pHead->pNext = NULL;
			}
			else
			{
				Node *cur = l.pHead;
				while (cur->pNext != NULL)
					cur = cur->pNext;
				cur->pNext = new Node;
				cur->pNext->parameter = tmp;
				cur->pNext->n = cur1->n;
				cur->pNext->variable = new char[cur1->n];
				cur->pNext->index_number = new int[cur1->n];
				for (int i = 0; i < cur1->n; i++)
				{
					cur->pNext->variable[i] = cur1->variable[i];
					cur->pNext->index_number[i] = cur1->index_number[i];
				}
				cur = cur->pNext;
				cur->pNext = NULL;
			}
			cur1 = cur1->pNext;
		}
	}
	if (cur1->flag == false) {
		Node *cur = l.pHead;
		while (cur->pNext != NULL)
			cur = cur->pNext;
		cur->pNext = new Node;
		cur->pNext->parameter = cur1->parameter;
		cur->pNext->n = cur1->n;
		cur->pNext->variable = new char[cur1->n];
		cur->pNext->index_number = new int[cur1->n];
		for (int i = 0; i < cur1->n; i++)
		{
			cur->pNext->variable[i] = cur1->variable[i];
			cur->pNext->index_number[i] = cur1->index_number[i];
		}
		cur = cur->pNext;
		cur->pNext = NULL;
	}
	l.deleteKey(0);

	*this = l;
	return *this;
}

// Vinh

Node* result;
Node* multi2Node(Node* A, Node* B)
{
	result->parameter = A->parameter*B->parameter;
	result->variable = NULL;
	result->index_number = NULL;
	result->n = 0;
	result->pNext = NULL;
	for (int i = 0; i < A->n; ++i) {
		add(result->variable, result->n, A->variable[i]);
		int tmp = result->n - 1;
		add(result->index_number, tmp, A->index_number[i]);
	}
	for (int i = 0; i < B->n; ++i) {
		add(result->variable, result->n, B->variable[i]);
		int tmp = result->n - 1;
		add(result->index_number, tmp, B->index_number[i]);
	}
	return result;
}


Polynomial Polynomial::multi2Polynomial(Polynomial a)
{
	Polynomial tmp = *this;
	Polynomial result;
	Node* F1 = this->pHead;
	Node* F2;
	while (F1 != NULL) {
		F2 = a.pHead;
		while (F2 != NULL) {
			result.insert(multi2Node(F1, F2));
			F2 = F2->pNext;
		}
		F1 = F1->pNext;
	}
	result.compactPolynomial();
	result.StandardizePoly();
	return result;
}
