#pragma once
#ifndef _POLYNOMIAL_
#define _POLYNOMIAL_
#include <iostream>
#include <fstream>

using namespace std;

const string file_f1 = "F1.txt";
const string file_f2 = "F2.txt";
const string file_r = "F-result.txt";

struct Node // Mot node luu mot don thuc
{
	float parameter; // He so
	char* variable; // Bien so
	int* index_number; // So mu
	int n; // So luong bien so
	bool flag = false;
	Node* pNext;
};

class Polynomial // Luu mot da thuc
{
private:
	Node *pHead;
public:
	Polynomial();
	void insert(Node* m);
	void input(ifstream &file);
	void ouput(ofstream &file);
	void swap(Node *&p1, Node *&p2);
	Polynomial& StandardizePoly();
	Polynomial& operator=(Polynomial a);
	Polynomial add2Polynomial(Polynomial a);
	Polynomial subtract2Polynomial(Polynomial a);
	void deleteKey(int key);
	bool isThatType(Node *&a, Node *&b);
	Polynomial& compactPolynomial();
	Polynomial multi2Polynomial(Polynomial a);
};

char preview(ifstream &file);
template<class T> void add(T* &arr, int &n, T e);

#endif